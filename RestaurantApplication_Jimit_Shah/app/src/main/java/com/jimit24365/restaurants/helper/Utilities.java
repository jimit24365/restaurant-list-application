
package com.jimit24365.restaurants.helper;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

public class Utilities {

	public static int screenWidthPixels,screenHeightPixels,deviceHeight,listCellHeight,listCellWidth,textSizeLarge,textSizeMedium,textSizeSmall;

	private static final String TAG = Utilities.class.getSimpleName();

	public static int dpToPx(float px,Context context){
		int pixels;
		if(context!=null){
			final float scale = context.getResources().getDisplayMetrics().density;
			pixels = (int) (px * scale + 0.5f);
		}else{
			pixels = (int) px;
            Log.e(TAG, "pxToDp failed - context null- returning px instead of dp");
		}
		return pixels;
	}

	public static void setGridContainerHeight(RecyclerView recyclerView, int numberOfBooks, int booksPerRow, int heightOfBook, int verticalPadding, int footerHeight) {
		int multiplier = (int) Math.ceil((float) numberOfBooks / (float) booksPerRow);
		HeightAnimation heightAnimation = new HeightAnimation(recyclerView,recyclerView.getMeasuredHeight(),(heightOfBook * multiplier) + footerHeight);
		heightAnimation.setDuration(500);
		recyclerView.startAnimation(heightAnimation);
	}


}
