package com.jimit24365.restaurants.helper;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.jimit24365.restaurants.R;
import com.jimit24365.restaurants.model.Chain;
import com.jimit24365.restaurants.model.RestaurantDetails;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by jimit on 10/11/16.
 */

public class RestaurantListAdapter extends RecyclerView.Adapter<RestaurantListAdapter.MyViewHolder> {

    private static final String RESTAURANT_IMAGE_BASE_URL = "https://res.cloudinary.com/swiggy/image/upload/";
    private final int LEVEL_ONE_HEIGHT = 100;
    private final int LEVEL_TWO_HEIGHT = 125;
    private Context mContext;
    private ArrayList<RestaurantDetails> restaurantDetailsList;
    private HashMap<Integer,Integer> listHeightLevelMap;

    public RestaurantListAdapter(Context context, ArrayList<RestaurantDetails> restaurantDetailsList,HashMap<Integer,Integer> listHeightLevelMap) {
        if(this.listHeightLevelMap == null){
            listHeightLevelMap = new HashMap<>();
        }
        this.mContext = context;
        this.restaurantDetailsList = restaurantDetailsList;
        this.listHeightLevelMap = listHeightLevelMap;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View weatherView = inflater.inflate(R.layout.restaurant_list_item_layout, null);
        MyViewHolder vh =
                new MyViewHolder(weatherView);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        RestaurantDetails currentRestaurantDetails = restaurantDetailsList.get(position);
        holder.populateRestaurantsDetails(currentRestaurantDetails, position);
    }

    @Override
    public int getItemCount() {
        return restaurantDetailsList == null || restaurantDetailsList.isEmpty() ? 0 : restaurantDetailsList.size();
    }

    /**
     * This method will add more data when list is completely visible and notify adapter the same.
     *
     * @param dataArray List of new restaurants
     */
    public void addData(ArrayList<RestaurantDetails> dataArray) {
        int sizeBeforeAdd = restaurantDetailsList.size();
        restaurantDetailsList.addAll(dataArray);
        notifyItemRangeInserted(sizeBeforeAdd, restaurantDetailsList.size());
    }

    public HashMap<Integer, Integer> getListHeightLevelMap() {
        return listHeightLevelMap;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private static final int CHAIN_ITEM_BASIC_HEIGHT = 30;
        private static final int CHAIN_ITEM_MARGIN = 5;
        private ImageView restaurantIv;
        private ImageView ratingImage;
        private ImageView arrowBtn;
        private TextView nameTv;
        private TextView cuisineTv;
        private TextView cost_1;
        private TextView cost_2;
        private TextView cost_3;
        private TextView cost_4;
        private TextView ratingTv;
        private TextView waitingTv;
        private TextView totalChainsIndicatorTv;
        private RelativeLayout actionContainer;
        private LinearLayout chainContainer;
        private ViewGroup itemContainer;
        private int itemViewHeightLevel;
        private int mPosition;
        private int currentHeight;
        private String waitingTimeFormat;

        public MyViewHolder(View itemView) {
            super(itemView);
            itemContainer = (ViewGroup) itemView.findViewById(R.id.top_container);
            nameTv = (TextView) itemView.findViewById(R.id.restaurant_name);
            restaurantIv = (ImageView) itemView.findViewById(R.id.restaurant_img);
            ratingImage = (ImageView) itemView.findViewById(R.id.rating_img);
            arrowBtn = (ImageView) itemView.findViewById(R.id.arrow_btn);
            cuisineTv = (TextView) itemView.findViewById(R.id.restaurant_cuisines);
            cost_1 = (TextView) itemView.findViewById(R.id.cost_1);
            cost_2 = (TextView) itemView.findViewById(R.id.cost_2);
            cost_3 = (TextView) itemView.findViewById(R.id.cost_3);
            cost_4 = (TextView) itemView.findViewById(R.id.cost_4);
            totalChainsIndicatorTv = (TextView) itemView.findViewById(R.id.total_chains_tv);
            ratingTv = (TextView) itemView.findViewById(R.id.rating);
            waitingTv = (TextView) itemView.findViewById(R.id.waiting);
            actionContainer = (RelativeLayout) itemView.findViewById(R.id.action_container);
            chainContainer = (LinearLayout) itemView.findViewById(R.id.chain_container);
            waitingTimeFormat = mContext.getResources().getString(R.string.waiting_time_format);
        }

        /**
         * This method will set listener for more chain indicator
         *
         * @param restaurantDetails Details of current restaurant
         */
        public void setActionPanelClickListener(final RestaurantDetails restaurantDetails) {
            actionContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    updateItemViewHeight(restaurantDetails, true);
                    updateChainContainerUI();
                }
            });
        }

        /**
         * This method will initialise all details of current restaurant item.
         *
         * @param restaurantDetails
         * @param position
         */
        public void populateRestaurantsDetails(RestaurantDetails restaurantDetails, int position) {
            if (restaurantDetails != null) {
                itemViewHeightLevel = 0;
                mPosition = position;
                chainContainer.removeAllViews();
                setRestaurantName(restaurantDetails);
                setCuisineDetails(restaurantDetails);
                setCostForTwo(restaurantDetails);
                setRatingDetails(restaurantDetails);
                setWaitingDetails(restaurantDetails);
                setRestaurantCoverArt(restaurantDetails);
                setActionPanelClickListener(restaurantDetails);
                if(listHeightLevelMap != null && listHeightLevelMap.containsKey(mPosition)){
                    itemViewHeightLevel = listHeightLevelMap.get(mPosition);
                    updateItemViewHeightForPosition(restaurantDetails);
                }else{
                    updateItemViewHeight(restaurantDetails, false);
                }
                updateChainContainerUI();
            }
        }

        /**
         * This method will restore height of item based on last user action to have better UX
         * @param restaurantDetails Details of current Restaurant item.
         */
        private void updateItemViewHeightForPosition(RestaurantDetails restaurantDetails) {
            ArrayList<Chain> chains = (ArrayList<Chain>) restaurantDetails.getChains();
            int totalChains = 0;
            if(chains != null) {
             totalChains = chains.size();
            }
            String totalChainIndicatorFormat =
                    mContext.getResources().getString(R.string.other_chains_indicator_msg);
            totalChainsIndicatorTv.setText(String.format(totalChainIndicatorFormat, totalChains + ""));
            switch (itemViewHeightLevel) {
                case 0:
                    currentHeight = LEVEL_ONE_HEIGHT;
                    break;
                case 1:
                    currentHeight = LEVEL_TWO_HEIGHT;
                    break;
                case 2:
                    currentHeight = LEVEL_TWO_HEIGHT + (totalChains * CHAIN_ITEM_BASIC_HEIGHT) + CHAIN_ITEM_MARGIN;
                    populateCurrentViewChains(restaurantDetails);
                    break;
            }
            int height = Utilities.dpToPx(currentHeight, itemContainer.getContext());
            ViewGroup.LayoutParams params = itemContainer.getLayoutParams();
            params.height = height;
            itemContainer.setLayoutParams(params);
        }

        /**
         * This method will update restaurant coverArt .
         *
         * @param restaurantDetails Details of current restaurant item
         */
        private void setRestaurantCoverArt(RestaurantDetails restaurantDetails) {
            String imageId = restaurantDetails.getImageId();
            if (!TextUtils.isEmpty(imageId)) {
                Glide.with(mContext).load(RESTAURANT_IMAGE_BASE_URL + imageId).into(restaurantIv);
            }
        }

        /**
         * This method will update name of current restaurant
         *
         * @param restaurantDetails Details of current restaurant item
         */
        private void setRestaurantName(RestaurantDetails restaurantDetails) {
            if (!TextUtils.isEmpty(restaurantDetails.getRestaurantName())) {
                nameTv.setText(restaurantDetails.getRestaurantName());
            }
        }

        /**
         * This method will update cuisine of current restaurant.
         *
         * @param restaurantDetails Details of current restaurant item
         */
        private void setCuisineDetails(RestaurantDetails restaurantDetails) {
            if (restaurantDetails.getCuisines() != null && !restaurantDetails.getCuisines().isEmpty()) {
                String cuisines = "";
                int size = restaurantDetails.getCuisines().size();
                for (int i = 0; i < size; i++) {
                    cuisines += restaurantDetails.getCuisines().get(i);
                    if (i != (size - 1)) {
                        cuisines += ", ";
                    }
                }
                cuisineTv.setText(cuisines);
            }
        }

        /**
         * This method will change color of rupee symbols based on costing
         *
         * @param restaurantDetails Details of current restaurant item
         */
        private void setCostForTwo(RestaurantDetails restaurantDetails) {
            String costForTwo = restaurantDetails.getCostForTwo();
            int cost = 0;
            int nonSelectedColor = mContext.getResources().getColor(android.R.color.darker_gray);
            int selectedColor = mContext.getResources().getColor(android.R.color.holo_blue_dark);
            if (!TextUtils.isEmpty(costForTwo)) {
                cost = costForTwo.length();
            }
            switch (cost) {
                case 0:
                    cost_1.setTextColor(nonSelectedColor);
                    cost_2.setTextColor(nonSelectedColor);
                    cost_3.setTextColor(nonSelectedColor);
                    cost_4.setTextColor(nonSelectedColor);
                    break;
                case 1:
                    cost_1.setTextColor(selectedColor);
                    cost_2.setTextColor(nonSelectedColor);
                    cost_3.setTextColor(nonSelectedColor);
                    cost_4.setTextColor(nonSelectedColor);
                    break;
                case 2:
                    cost_1.setTextColor(selectedColor);
                    cost_2.setTextColor(selectedColor);
                    cost_3.setTextColor(nonSelectedColor);
                    cost_4.setTextColor(nonSelectedColor);
                    break;
                case 3:
                    cost_1.setTextColor(selectedColor);
                    cost_2.setTextColor(selectedColor);
                    cost_3.setTextColor(selectedColor);
                    cost_4.setTextColor(nonSelectedColor);
                    break;
                case 4:
                    cost_1.setTextColor(selectedColor);
                    cost_2.setTextColor(selectedColor);
                    cost_3.setTextColor(selectedColor);
                    cost_4.setTextColor(selectedColor);
                    break;
            }
        }

        /**
         * This method will set rating of current restaurant
         *
         * @param restaurantDetails Details of current restaurant item
         */
        private void setRatingDetails(RestaurantDetails restaurantDetails) {
            if (!TextUtils.isEmpty(restaurantDetails.getRating())) {
                ratingTv.setText(restaurantDetails.getRating());
                ratingTv.setVisibility(View.VISIBLE);
                ratingImage.setVisibility(View.VISIBLE);
            } else {
                ratingTv.setVisibility(View.INVISIBLE);
                ratingImage.setVisibility(View.INVISIBLE);
            }
        }

        /**
         * This method will update delivery time taken by current restaurant
         *
         * @param restaurantDetails
         */
        private void setWaitingDetails(RestaurantDetails restaurantDetails) {
            if (!TextUtils.isEmpty(restaurantDetails.getDeliveryTime() + "")) {
                waitingTimeFormat = String.format(waitingTimeFormat, restaurantDetails.getDeliveryTime() + "");
                waitingTv.setText(waitingTimeFormat);
            }

        }

        /**
         * This method will update UI of other chains button and chain details container.
         */
        private void updateChainContainerUI() {
            switch (itemViewHeightLevel) {
                case 0:
                    actionContainer.setVisibility(View.INVISIBLE);
                    chainContainer.setVisibility(View.INVISIBLE);
                    break;
                case 1:
                    actionContainer.setVisibility(View.VISIBLE);
                    arrowBtn.setImageResource(R.drawable.arrow_down);
                    chainContainer.setVisibility(View.INVISIBLE);
                    break;
                case 2:
                    actionContainer.setVisibility(View.VISIBLE);
                    arrowBtn.setImageResource(R.drawable.arrow_up);
                    chainContainer.setVisibility(View.VISIBLE);
                    break;
            }
        }

        /**
         * This method will update chains details view with or without animation
         *
         * @param restaurantDetails Details of current restaurant item
         * @param withAnimation     Specifies whether to animation while updating view or not
         */
        private void updateItemViewHeight(RestaurantDetails restaurantDetails, boolean withAnimation) {
            ArrayList<Chain> chains = (ArrayList<Chain>) restaurantDetails.getChains();
            int height;
            int expandedHeight = 0;
            int collapsedHeight = 0;
            boolean shouldExpand = false;
            if (chains == null || chains.isEmpty()) {
                itemViewHeightLevel = 0;
                expandedHeight = LEVEL_ONE_HEIGHT;
                currentHeight = expandedHeight;
            } else {
                int totalChains = chains.size();
                String totalChainIndicatorFormat =
                        mContext.getResources().getString(R.string.other_chains_indicator_msg);
                totalChainsIndicatorTv.setText(String.format(totalChainIndicatorFormat, totalChains + ""));
                switch (itemViewHeightLevel) {
                    case 0:
                        itemViewHeightLevel++;
                        currentHeight = LEVEL_TWO_HEIGHT;
                        expandedHeight = currentHeight;
                        break;
                    case 1:
                        itemViewHeightLevel++;
                        expandedHeight = currentHeight + (totalChains * CHAIN_ITEM_BASIC_HEIGHT) + CHAIN_ITEM_MARGIN;
                        collapsedHeight = currentHeight;
                        currentHeight = expandedHeight;
                        shouldExpand = true;
                        break;
                    case 2:
                        itemViewHeightLevel--;
                        expandedHeight = currentHeight;
                        collapsedHeight = LEVEL_TWO_HEIGHT;
                        currentHeight = collapsedHeight;
                        shouldExpand = false;
                        break;
                }
            }
            listHeightLevelMap.put(mPosition,itemViewHeightLevel);
            if (!withAnimation) {
                height = Utilities.dpToPx(expandedHeight, itemContainer.getContext());
                ViewGroup.LayoutParams params = itemContainer.getLayoutParams();
                params.height = height;
                itemContainer.setLayoutParams(params);
            } else {
                showPanelAnimation(restaurantDetails, shouldExpand, expandedHeight, collapsedHeight);
            }
        }

        /**
         * This method will change current view height with "vertical drawer" style animation and
         * will scroll current item to top of the recyclerView(parent list) for better UX.
         *
         * @param restaurantDetails Details of current restaurant view
         * @param shouldExpand      Specifies whether to increase height or to decrease height of view.
         * @param currentHeight     Current height of the view in dp
         * @param expectedHeight    Final height of the view in dp
         */
        private void showPanelAnimation(final RestaurantDetails restaurantDetails, final Boolean shouldExpand, int currentHeight, int expectedHeight) {
            int endingHeight;
            int startingHeight;
            if (shouldExpand) {
                endingHeight = Utilities.dpToPx(currentHeight, itemView.getContext());
                startingHeight = Utilities.dpToPx(expectedHeight, itemView.getContext());
                RecyclerView recyclerView = ((RecyclerView) itemView.getParent());
                SmoothScrollLinearLayoutManager linearLayoutManager = ((SmoothScrollLinearLayoutManager) recyclerView.getLayoutManager());
                linearLayoutManager.smoothScrollToPosition(recyclerView, new RecyclerView.State(), mPosition);
            } else {
                endingHeight = Utilities.dpToPx(expectedHeight, itemView.getContext());
                startingHeight = Utilities.dpToPx(currentHeight, itemView.getContext());
            }
            HeightAnimation heightAnimation = new HeightAnimation(itemContainer, startingHeight, endingHeight);
            heightAnimation.setDuration(200);
            heightAnimation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    chainContainer.removeAllViews();
                    if (shouldExpand) {
                        populateCurrentViewChains(restaurantDetails);
                    }
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            itemContainer.startAnimation(heightAnimation);
        }

        /**
         * This method will populate all chains of current restaurants in linearLayout container
         * after validating details from restaurantDetails
         *
         * @param restaurantDetails Details of current restaurant item.
         */
        private void populateCurrentViewChains(RestaurantDetails restaurantDetails) {
            if(restaurantDetails.getChains() != null){
                int totalChains = restaurantDetails.getChains().size();
                for (int i = 0; i < totalChains; i++) {

                    String waitingTime = String.valueOf(
                            restaurantDetails.getChains().get(i).getDeliveryTime());
                    String rating = restaurantDetails.getChains().get(i).getRating();
                    String location = restaurantDetails.getChains().get(i).getArea();

                    LayoutInflater inflater = (LayoutInflater) mContext
                            .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View chainView = inflater.inflate(R.layout.chain_item_container, null);
                    TextView locationTv = ((TextView) chainView.findViewById(R.id.location));
                    TextView ratingTv = ((TextView) chainView.findViewById(R.id.rating));
                    if (!TextUtils.isEmpty(location)) {
                        locationTv.setText(location);
                    } else {
                        locationTv.setText("");
                    }

                    if (!TextUtils.isEmpty(rating)) {
                        ratingTv.setText(rating);
                    } else {
                        ratingTv.setText("");
                    }

                    if (!TextUtils.isEmpty(waitingTime)) {
                        ((TextView) chainView.findViewById(R.id.waiting))
                                .setText(String.format(waitingTimeFormat, waitingTime));
                    } else {
                        ((TextView) chainView.findViewById(R.id.waiting))
                                .setText(String.format(waitingTimeFormat, "0"));
                    }
                    chainContainer.addView(chainView);
                }
                chainContainer.requestLayout();
            }
        }


    }

}

