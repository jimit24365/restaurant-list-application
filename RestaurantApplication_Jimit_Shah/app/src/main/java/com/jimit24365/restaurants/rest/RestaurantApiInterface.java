package com.jimit24365.restaurants.rest;

import com.jimit24365.restaurants.model.RestaurantList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by jimit on 9/11/16.
 */

public interface RestaurantApiInterface {

    @GET("ngcc")
    Call<RestaurantList> getRestaurants(@Query("page") int q);
}
