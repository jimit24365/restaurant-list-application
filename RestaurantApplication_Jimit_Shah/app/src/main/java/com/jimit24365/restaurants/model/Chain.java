package com.jimit24365.restaurants.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by jimit on 9/11/16.
 */

public class Chain {

    @SerializedName("name")
    private String restaurantName;
    @SerializedName("city")
    private String city;
    @SerializedName("area")
    private String area;
    @SerializedName("avg_rating")
    private String rating;
    @SerializedName("cid")
    private String imageId;
    @SerializedName("costForTwo")
    private String costForTwo;
    @SerializedName("deliveryTime")
    private Integer deliveryTime;
    @SerializedName("closed")
    private boolean isOpenNow;
    @SerializedName("cuisine")
    private List<String> cuisines;


    public String getRestaurantName() {
        return restaurantName;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public String getCostForTwo() {
        return costForTwo;
    }

    public void setCostForTwo(String costForTwo) {
        this.costForTwo = costForTwo;
    }

    public Integer getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(Integer deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public boolean isOpenNow() {
        return isOpenNow;
    }

    public void setOpenNow(boolean openNow) {
        isOpenNow = openNow;
    }

    public List<String> getCuisines() {
        return cuisines;
    }

    public void setCuisines(List<String> cuisines) {
        this.cuisines = cuisines;
    }
}
