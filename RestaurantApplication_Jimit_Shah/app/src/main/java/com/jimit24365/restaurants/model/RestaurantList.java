package com.jimit24365.restaurants.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by jimit on 9/11/16.
 */

public class RestaurantList {

    @SerializedName("restaurants")
    private List<RestaurantDetails> restaurantDetails;

    public List<RestaurantDetails> getRestaurantDetails() {
        return restaurantDetails;
    }

    public void setRestaurantDetails(List<RestaurantDetails> restaurantDetails) {
        this.restaurantDetails = restaurantDetails;
    }
}
