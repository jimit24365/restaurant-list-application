package com.jimit24365.restaurants.activity;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.jimit24365.restaurants.R;
import com.jimit24365.restaurants.helper.RestaurantListAdapter;
import com.jimit24365.restaurants.helper.SmoothScrollLinearLayoutManager;
import com.jimit24365.restaurants.model.RestaurantDetails;
import com.jimit24365.restaurants.model.RestaurantList;
import com.jimit24365.restaurants.rest.RestaurantApiClient;
import com.jimit24365.restaurants.rest.RestaurantApiInterface;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private static final int MAX_PAGE_THRESHOLD = 100;
    private static final int MIN_RESTAURANTS_TO_START_LOADING_MORE_RESTAURANT = 2;
    private static final String TAG = MainActivity.class.getSimpleName();

    private static final String KEY_LIST_COUNT_BEFORE_LOADING_MORE_STATE = "list_count_before_loading_more";
    private static final String KEY_LAST_PAGE_REQUESTED_STATE = "list_count_before_loading_more";
    private static final String KEY_IS_LOADING_MORE_RESTAURANT_STATE = "is_loading_more_restaurant";
    private static final String KEY_IS_ALL_RESTAURANT_LOADED_STATE = "is_all_restaurant_loaded";
    private static final String LIST_STATE_KEY = "recycler_view_list_state";
    private static final String RECYCLER_VIEW_HEIGHT_LEVEL_STATE = "height_level_map_state";


    private RecyclerView recyclerView;
    private View footerLoadingIndicator;
    private View mainLoadingIndicator;
    private TextView errorMessageTv;

    private int listCountBeforeLoadingMore;
    private int lastPageRequested;
    private boolean isLoadingMoreRestaurants;
    private boolean allRestaurantsLoaded = false;

    private Parcelable mRestaurantListState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = (RecyclerView) findViewById(R.id.restaurant_list);
        recyclerView.setLayoutManager(new SmoothScrollLinearLayoutManager(MainActivity.this, LinearLayoutManager.VERTICAL, false));
        footerLoadingIndicator = findViewById(R.id.loading_indicator);
        mainLoadingIndicator = findViewById(R.id.main_loading_indicator);
        errorMessageTv = (TextView) findViewById(R.id.error_fetching_tv);

        // updating more restaurants on scroll of recyclerView
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (!allRestaurantsLoaded) {
                    loadMoreRestaurants();
                }
            }
        });
        if (savedInstanceState == null) {
            fetchAndPoppulateRestaurants(true, lastPageRequested++,null);
        } else {
            unPackAndRestoreState(savedInstanceState);
        }
    }


    /**
     * This method will start loading more restaurants in recyclerviw after checking
     * valid stage( just before completion of list)
     */
    private void loadMoreRestaurants() {
        SmoothScrollLinearLayoutManager linearLayoutManager =
                (SmoothScrollLinearLayoutManager) recyclerView.getLayoutManager();
        if (linearLayoutManager.findLastCompletelyVisibleItemPosition()
                >= linearLayoutManager.getItemCount() - MIN_RESTAURANTS_TO_START_LOADING_MORE_RESTAURANT
                &&
                !isLoadingMoreRestaurants && listCountBeforeLoadingMore
                != linearLayoutManager.getItemCount()) {
            listCountBeforeLoadingMore = linearLayoutManager.getItemCount();
            showListFooterProgress(true);
            isLoadingMoreRestaurants = true;
            fetchAndPoppulateRestaurants(false, lastPageRequested++,null);
        }
    }

    /**
     * This method will show/hide loading indicator at bottom of recyclerView.
     *
     * @param shouldShow Specifies whether to show or hide loading indicator
     */
    private void showListFooterProgress(boolean shouldShow) {
        if (shouldShow) {
            footerLoadingIndicator.setVisibility(View.VISIBLE);
        } else {
            footerLoadingIndicator.setVisibility(View.GONE);
        }
    }

    /**
     * This method will act as callback indicating to start background task of fetching new page data.
     *
     * @param isLoadingFirstTime Specifies whether requesting restaurant list for 1st time or not
     */
    private void fetchAndPoppulateRestaurants(final boolean isLoadingFirstTime, int pageRequested, final HashMap<Integer,Integer> listHeightLevelMap) {
        if (isLoadingFirstTime) {
            mainLoadingIndicator.setVisibility(View.VISIBLE);
        }
        RestaurantApiInterface apiInterface =
                RestaurantApiClient.getClient().create(RestaurantApiInterface.class);
        Call<RestaurantList> call = apiInterface.getRestaurants(pageRequested);
        call.enqueue(new Callback<RestaurantList>() {
            @Override
            public void onResponse(Call<RestaurantList> call, Response<RestaurantList> response) {
                RestaurantList restaurantList = response.body();
                Log.d("MainActivity", restaurantList.getRestaurantDetails().get(0).getCuisines().get(0));
                if (isLoadingFirstTime) {
                    setData((ArrayList<RestaurantDetails>) restaurantList.getRestaurantDetails(),listHeightLevelMap);
                } else {
                    addItemsInList((ArrayList<RestaurantDetails>) restaurantList.getRestaurantDetails());
                }
                if (isLoadingFirstTime) {
                    mainLoadingIndicator.setVisibility(View.GONE);
                    errorMessageTv.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<RestaurantList> call, Throwable t) {
                allRestaurantsLoaded = true;
                if (isLoadingFirstTime) {
                    mainLoadingIndicator.setVisibility(View.GONE);
                    errorMessageTv.setVisibility(View.VISIBLE); // shows error message in condition like no network connectivity
                }
                onLoadingMoreItemsComplete();
            }
        });
    }

    /**
     * This method will add new list of restaurants in existing list of adapter
     *
     * @param newData List of restaurants
     */
    protected final void addItemsInList(ArrayList<RestaurantDetails> newData) {
        if (recyclerView.getAdapter() == null) {
            return;
        }
        ((RestaurantListAdapter) recyclerView.getAdapter()).addData(newData);
        allRestaurantsLoaded = newData == null || lastPageRequested == MAX_PAGE_THRESHOLD;
        onLoadingMoreItemsComplete();
    }

    /**
     * This method will act as a callback to notify UI about completion of new page data fetching
     */
    protected final void onLoadingMoreItemsComplete() {
        isLoadingMoreRestaurants = false;
        showListFooterProgress(false);
    }

    /**
     * This method will set list of restaurants in recyclerview
     *
     * @param dataArray List of restaurants
     */
    protected void setData(ArrayList<RestaurantDetails> dataArray,HashMap<Integer,Integer> listHeightLevelMap) {
        allRestaurantsLoaded = false;
        listCountBeforeLoadingMore = -1;
        recyclerView.setAdapter(new RestaurantListAdapter(MainActivity.this, dataArray,listHeightLevelMap));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        packInstanceState(outState);
    }

    /**
     * Packing current screen various non system saved view state like recycler view and various
     * class members
     * @param outState
     */
    private void packInstanceState(Bundle outState) {

        outState.putInt(KEY_LIST_COUNT_BEFORE_LOADING_MORE_STATE, listCountBeforeLoadingMore);
        outState.putInt(KEY_LAST_PAGE_REQUESTED_STATE, lastPageRequested);
        outState.putBoolean(KEY_IS_ALL_RESTAURANT_LOADED_STATE, allRestaurantsLoaded);
        outState.putBoolean(KEY_IS_LOADING_MORE_RESTAURANT_STATE, isLoadingMoreRestaurants);
        if(recyclerView != null &&  recyclerView.getLayoutManager() != null) {
            RestaurantListAdapter restaurantListAdapter = (RestaurantListAdapter) recyclerView.getAdapter();
            if(restaurantListAdapter != null){
                outState.putSerializable(RECYCLER_VIEW_HEIGHT_LEVEL_STATE,restaurantListAdapter.getListHeightLevelMap());
            }
            outState.putParcelable(LIST_STATE_KEY, recyclerView.getLayoutManager().onSaveInstanceState());
        }
    }

    /**
     * Un packing bundle and restoring various state of views such that recycler view list state along
     * with various class members.
     * @param savedInstanceState
     */
    private void unPackAndRestoreState(Bundle savedInstanceState) {
        Parcelable listState = savedInstanceState.getParcelable(LIST_STATE_KEY);
        HashMap<Integer,Integer> recyclerViewHeightLevelMap = null;
        recyclerViewHeightLevelMap = (HashMap<Integer, Integer>)
                savedInstanceState.getSerializable(RECYCLER_VIEW_HEIGHT_LEVEL_STATE);
        listCountBeforeLoadingMore = savedInstanceState.getInt(KEY_LIST_COUNT_BEFORE_LOADING_MORE_STATE);
        lastPageRequested = savedInstanceState.getInt(KEY_LAST_PAGE_REQUESTED_STATE);
        allRestaurantsLoaded = savedInstanceState.getBoolean(KEY_IS_ALL_RESTAURANT_LOADED_STATE);
        isLoadingMoreRestaurants = savedInstanceState.getBoolean(KEY_IS_LOADING_MORE_RESTAURANT_STATE);
        for (int i = 0; i < lastPageRequested; i++) {
            fetchAndPoppulateRestaurants(i == 0 ? true : false, i,recyclerViewHeightLevelMap);
        }
        if(listState != null) {
            recyclerView.getLayoutManager().onRestoreInstanceState(listState);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(recyclerView != null &&  recyclerView.getLayoutManager() != null) {
            mRestaurantListState = recyclerView.getLayoutManager().onSaveInstanceState();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(mRestaurantListState != null){
            recyclerView.getLayoutManager().onRestoreInstanceState(mRestaurantListState);
        }
    }
}
