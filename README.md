# README #

## Restaurant List Application ##

### Features ###

* Fetching restaurants list from API
* Auto-updating list on reaching end of list(Network call is started to get next page data on reaching 2nd last element of list for better UX)
* Scrolling list element to top of screen when expanded for better UX (So that user can see all chains of restaurant)
* Restoring recyclerView state onOrientation and ActivityDestruction